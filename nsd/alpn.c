/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "nsd.h"

#ifdef OPENSSL_HAS_ALPN_SUPPORT

#include <string.h>

/**
 * Protocols used for for alpnSelectCallbackWithHttp2AndHttp11.
 * Before each string a char is used as an integer to represent the length of the ALPN constant.
 */
static const unsigned char HTTP2_AND_HTTP1_1_PROTOCOLS[] = {
        2u, 'h', '2',
        8u, 'h', 't', 't', 'p', '/', '1', '.', '1'
};

int alpnSelectCallbackWithHttp2(SSL *UNUSED(ssl),
                                const unsigned char **out,
                                unsigned char *outlen,
                                const unsigned char *in,
                                unsigned int inlen,
                                void *UNUSED(arg)) {
    switch (SSL_select_next_proto((unsigned char **) out,
                                  outlen,
                                  in,
                                  inlen,
                                  HTTP2_AND_HTTP1_1_PROTOCOLS,
                                  3u)) {
        case OPENSSL_NPN_NEGOTIATED:
            return SSL_TLSEXT_ERR_OK;
        case OPENSSL_NPN_NO_OVERLAP:
            return SSL_TLSEXT_ERR_ALERT_FATAL;
        default:
            return EXIT_FAILURE;
    }
}

int alpnSelectCallbackWithHttp2AndHttp11(SSL *UNUSED(ssl),
                                         const unsigned char **out,
                                         unsigned char *outlen,
                                         const unsigned char *in,
                                         unsigned int inlen,
                                         void *UNUSED(arg)) {
    switch (SSL_select_next_proto((unsigned char **) out,
                                  outlen,
                                  in,
                                  inlen,
                                  HTTP2_AND_HTTP1_1_PROTOCOLS,
                                  12u)) {
        case OPENSSL_NPN_NEGOTIATED:
            return SSL_TLSEXT_ERR_OK;
        case OPENSSL_NPN_NO_OVERLAP:
            return SSL_TLSEXT_ERR_ALERT_FATAL;
        default:
            return EXIT_FAILURE;
    }
}

int alpnSelectCallbackWithHttp11(SSL *UNUSED(ssl),
                                 const unsigned char **out,
                                 unsigned char *outlen,
                                 const unsigned char *in,
                                 unsigned int inlen,
                                 void *UNUSED(arg)) {
    switch (SSL_select_next_proto((unsigned char **) out,
                                  outlen,
                                  in,
                                  inlen,
                                  HTTP2_AND_HTTP1_1_PROTOCOLS + 3u,
                                  9u)) {
        case OPENSSL_NPN_NEGOTIATED:
            return SSL_TLSEXT_ERR_OK;
        case OPENSSL_NPN_NO_OVERLAP:
            return SSL_TLSEXT_ERR_ALERT_FATAL;
        default:
            return EXIT_FAILURE;
    }
}

void alpnSetupProtocolHandler(SSL_CTX *ctx,
                              const uint8_t protocolSettings) {
    switch (protocolSettings) {
        case ALPN_HTTP1_1:
            SSL_CTX_set_alpn_select_cb(ctx, alpnSelectCallbackWithHttp11, NULL);
            break;
        case ALPN_HTTP2:
            SSL_CTX_set_alpn_select_cb(ctx, alpnSelectCallbackWithHttp2, NULL);
            break;
        case ALPN_HTTP2_HTTP1_1:
            SSL_CTX_set_alpn_select_cb(ctx, alpnSelectCallbackWithHttp2AndHttp11, NULL);
            break;
        default:
            SSL_CTX_set_alpn_select_cb(ctx, alpnSelectCallbackWithHttp11, NULL);
    }
}

uint8_t alpnGetSelectedProtocol(const SSL *ssl) {
    const unsigned char *name;
    unsigned int len;

    SSL_get0_alpn_selected(ssl, &name, &len);

    switch (len) {
        case ALPN_HTTP2_STR_LEN:
            if (memcmp(ALPN_HTTP2_STR, (char *) name, len) == 0) {
                return ALPN_HTTP2;
            } else {
                return ALPN_NO_MATCH;
            }
        case ALPN_HTTP1_1_STR_LEN:
            if (memcmp(ALPN_HTTP1_1_STR, (char *) name, len) == 0) {
                return ALPN_HTTP1_1;
            } else {
                return ALPN_NO_MATCH;
            }
        default:
            return ALPN_NO_MATCH;
    }
}

char *alpnGetProtocolName(const uint8_t protocolIdentifier) {
    switch (protocolIdentifier) {
        case ALPN_HTTP2:
            return ALPN_HTTP2_STR;
        case ALPN_HTTP1_1:
            return ALPN_HTTP1_1_STR;
        default:
            return NULL;
    }
}

#endif
