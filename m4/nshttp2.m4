AC_DEFUN([AX_HAVE_NSHTTP2], [
AC_MSG_CHECKING([for nshttp2 library])
AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
        #include <nshttp2/connection.h>
    ]], [[
        Http2ConnectionT *connection = NULL;
        return 0;
    ]])], [
        AC_DEFINE([HAVE_NSHTTP2],1,[Define to 1 when nshttp2 library is available.])
        AC_MSG_RESULT([yes])
        NSHTTP2_LIBS="-lnghttp2 -lnshttp2"
        AC_SUBST([NSHTTP2_LIBS])
    ],[
        AC_MSG_RESULT([no])
        NSHTTP2_LIBS=""
        AC_SUBST([NSHTTP2_LIBS])
    ])
]) # AX_HAVE_NSHTTP2
